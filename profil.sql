-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 06, 2016 at 12:02 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `profil`
--

-- --------------------------------------------------------

--
-- Table structure for table `accreditations`
--

CREATE TABLE `accreditations` (
  `id` int(11) NOT NULL,
  `id_prodi` int(11) NOT NULL,
  `sk_number` varchar(128) NOT NULL,
  `accreditation` char(1) NOT NULL,
  `date_end` date NOT NULL,
  `image` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accreditations`
--

INSERT INTO `accreditations` (`id`, `id_prodi`, `sk_number`, `accreditation`, `date_end`, `image`) VALUES
(1, 30, 'ADIB/21/01/94', 'C', '2016-09-30', 'ZtLXY4ivjA.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `alumni_profiles`
--

CREATE TABLE `alumni_profiles` (
  `id` int(11) NOT NULL,
  `profesi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alumni_profiles`
--

INSERT INTO `alumni_profiles` (`id`, `profesi`) VALUES
(1, 'Programmer'),
(5, 'Engineer');

-- --------------------------------------------------------

--
-- Table structure for table `curiculums`
--

CREATE TABLE `curiculums` (
  `id` int(11) NOT NULL,
  `id_prodi` int(11) NOT NULL,
  `tujuan` text NOT NULL,
  `materi` text NOT NULL,
  `izin` varchar(128) NOT NULL,
  `gelar` varchar(64) NOT NULL,
  `kkni` varchar(128) NOT NULL,
  `kompetensi` text NOT NULL,
  `pembelajaran` text NOT NULL,
  `persyaratan` text NOT NULL,
  `dukungan` text NOT NULL,
  `syarat_admisi` text NOT NULL,
  `lain_lain` text NOT NULL,
  `video` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `curiculums`
--

INSERT INTO `curiculums` (`id`, `id_prodi`, `tujuan`, `materi`, `izin`, `gelar`, `kkni`, `kompetensi`, `pembelajaran`, `persyaratan`, `dukungan`, `syarat_admisi`, `lain_lain`, `video`) VALUES
(1, 1, '', 'a', '', '', '', '', '', '', '', '', '', ''),
(2, 157, '', 'deeeeeee\r\nd\r\ndeeee', 'a', 'b', 'c', 'e\r\ne\r\neasdasd', 'f\r\nfasds\r\nf', 'g\r\ngdasdas\r\ng', 'h\r\nhasdasd\r\nh', 'iasdsa\r\ni\r\n', 'jsaddas\r\nj\r\n', 'https://www.youtube.com/watch?v=D4MuNYqD9zQ'),
(3, 30, 'sadsadsadcxcvxc\r\nnnbmnmnm,ytutyuty', 'sadsad\r\nasdasdas\r\nfadsfa', 'aa', 'S.E.', 'asdas', 'dfdsdf', 'sdfsdf', 'sdff', 'cxvxv', 'htyyytu', 'dfgdfg', 'https://www.youtube.com/watch?v=D4MuNYqD9zQ'),
(4, 77, '<p>adasdasd</p>\r\n', '&lt;p&gt;dasfasfas&lt;/p&gt;\r\n', 'A', 'S.H.', 'sass', '&lt;p&gt;kjklkjlk&lt;/p&gt;\r\n', '&lt;p&gt;lkjlkjkljkl&lt;/p&gt;\r\n', '&lt;p&gt;mn,mn,mn&lt;/p&gt;\r\n', '&lt;p&gt;nm,n,mn,m&lt;/p&gt;\r\n', '&lt;p&gt;n,mn,mn,m&lt;/p&gt;\r\n', '&lt;p&gt;,mn,mnm,nm,&lt;/p&gt;\r\n', 'https://www.youtube.com/watch?v=D4MuNYqD9zQ'),
(5, 152, '<p>m</p>\r\n', '&lt;p&gt;m&lt;/p&gt;\r\n', 'm', 'A.Md.', 'kk', '&lt;p&gt;m&lt;/p&gt;\r\n', '&lt;p&gt;m&lt;/p&gt;\r\n', '&lt;p&gt;m&lt;/p&gt;\r\n', '&lt;p&gt;m&lt;/p&gt;\r\n', '&lt;p&gt;m&lt;/p&gt;\r\n', '&lt;p&gt;m&lt;/p&gt;\r\n', 'm');

-- --------------------------------------------------------

--
-- Table structure for table `curiculum_alumni`
--

CREATE TABLE `curiculum_alumni` (
  `id` int(11) NOT NULL,
  `id_curiculum` int(11) NOT NULL,
  `id_alumni_profile` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `curiculum_alumni`
--

INSERT INTO `curiculum_alumni` (`id`, `id_curiculum`, `id_alumni_profile`) VALUES
(1, 5, 1),
(2, 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `fakultas`
--

CREATE TABLE `fakultas` (
  `id_fakultas` int(11) NOT NULL,
  `kode_fakultas` varchar(20) NOT NULL,
  `nama_fakultas` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fakultas`
--

INSERT INTO `fakultas` (`id_fakultas`, `kode_fakultas`, `nama_fakultas`) VALUES
(1, '01', 'HUKUM'),
(2, '02', 'EKOMONIKA DAN BISNIS'),
(3, '03', 'TEKNIK'),
(4, '04', 'KEDOKTERAN'),
(5, '05', 'PETERNAKAN DAN PERTANIAN'),
(6, '06', 'ILMU BUDAYA'),
(7, '07', 'ILMU SOSIAL DAN ILMU POLITIK'),
(8, '08', 'KESEHATAN MASYARAKAT'),
(9, '09', 'SAINS DAN MATEMATIKA'),
(10, '10', 'PERIKANAN DAN ILMU KELAUTAN'),
(11, '11', 'PSIKOLOGI');

-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE `fees` (
  `id` int(11) NOT NULL,
  `id_prodi` int(11) NOT NULL,
  `jalur` varchar(16) NOT NULL,
  `ukt_1` int(11) NOT NULL,
  `ukt_2` int(11) NOT NULL,
  `ukt_3` int(11) NOT NULL,
  `ukt_4` int(11) NOT NULL,
  `ukt_5` int(11) NOT NULL,
  `ukt_6` int(11) NOT NULL,
  `ukt_7` int(11) NOT NULL,
  `spi_1` int(11) NOT NULL,
  `spi_2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fees`
--

INSERT INTO `fees` (`id`, `id_prodi`, `jalur`, `ukt_1`, `ukt_2`, `ukt_3`, `ukt_4`, `ukt_5`, `ukt_6`, `ukt_7`, `spi_1`, `spi_2`) VALUES
(1, 4, 'UM', 0, 0, 0, 0, 0, 6000000, 7000000, 20000000, 25000000),
(2, 4, 'SNMPTN', 500000, 1000000, 2000000, 3500000, 5000000, 6000000, 7000000, 0, 0),
(25, 47, 'aaa', 0, 0, 0, 0, 0, 0, 0, 0, 0),
(26, 43, 'SNMPTN', 1000, 2000, 3000, 4000, 5000, 6000, 7000, 0, 0),
(27, 43, 'UM', 0, 0, 0, 0, 0, 0, 7000, 10000, 20000);

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `id_jurusan` int(11) NOT NULL,
  `id_fakultas` varchar(11) NOT NULL,
  `kode_jurusan` varchar(11) NOT NULL,
  `nama_jurusan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `id_fakultas`, `kode_jurusan`, `nama_jurusan`) VALUES
(1, '4', '43014612', 'S1-PENDIDIKAN DOKTER'),
(2, '3', '43024312', 'S1-TEKNIK SIPIL'),
(3, '3', '43034712', 'S1-TEKNIK ARSITEKTUR'),
(4, '3', '43044412', 'S1-TEKNIK KIMIA'),
(5, '9', '43054112', 'S1-MATEMATIKA'),
(6, '3', '43064512', 'S1-TEKNIK MESIN'),
(7, '10', '43074212', 'S1-MANAJEMEN SD PERAIRAN'),
(8, '3', '43104512', 'S1-TEKNIK ELEKTRO'),
(9, '8', '43114212', 'S1-KESEHATAN MASYARAKAT'),
(10, '10', '43124612', 'S1-ILMU KELAUTAN'),
(11, '9', '43134312', 'S1-BIOLOGI'),
(12, '9', '43144712', 'S1-KIMIA'),
(13, '9', '43154412', 'S1-FISIKA'),
(14, '3', '43164112', 'S1-TEKNIK PERENC. WIL. & KOTA'),
(15, '10', '43184212', 'S1-BUDIDAYA PERAIRAN'),
(16, '10', '43194612', 'S1-PEMANFAATAN SD PERIKANAN'),
(17, '3', '43204112', 'S1-TEKNIK INDUSTRI'),
(18, '3', '43214512', 'S1-TEKNIK LINGKUNGAN'),
(19, '5', '43494112', 'S1-PETERNAKAN'),
(20, '4', '43244312', 'S1-ILMU KEPERAWATAN'),
(21, '10', '43254712', 'S1-OSEANOGRAFI'),
(22, '10', '43266412', 'S1-TEKNOLOGI HASIL PERIKANAN'),
(24, '7', '43334232', 'S1-ADMINISTRASI PUBLIK'),
(25, '7', '43344632', 'S1-ADMINISTRASI BISNIS'),
(26, '7', '43354332', 'S1-ILMU PEMERINTAHAN'),
(27, '7', '43364732', 'S1-ILMU KOMUNIKASI'),
(28, '6', '43374432', 'S1-ILMU SEJARAH'),
(29, '2', '43384132', 'S1-MANAJEMEN'),
(30, '2', '43394532', 'S1-AKUNTANSI'),
(31, '2', '43404732', 'S1-STUDI PEMBANGUNAN'),
(32, '11', '43414432', 'S1-PSIKOLOGI'),
(33, '4', '43424112', 'S1-ILMU GIZI'),
(34, '9', '43434512', 'S1-STATISTIKA'),
(35, '3', '43444212', 'S1-TEKNIK PERKAPALAN'),
(36, '3', '43454612', 'S1-TEKNIK GEOLOGI'),
(37, '3', '43464312', 'S1-TEKNIK GEODESI'),
(38, '9', '43474712', 'S1-TEKNIK INFORMATIKA'),
(39, '6', '43614332', 'S1-ILMU PERPUSTAKAAN'),
(40, '6', '43814232', 'S1-SASTRA INDONESIA'),
(41, '6', '43824632', 'S1-SASTRA INGGRIS'),
(42, '3', '43484412', 'S1-SISTEM KOMPUTER'),
(43, '6', '43824631', 'D3-BAHASA INGGRIS'),
(44, '6', '43714431', 'D3-KEARSIPAN'),
(45, '6', '43614331', 'D3-PERPUSTAKAAN & INFORMASI'),
(47, '2', '43394531', 'D3-AKUNTANSI'),
(48, '2', '43283431', 'D3-MANAJEMEN PERUSAHAAN'),
(49, '2', '43172331', 'D3-PERPAJAKAN'),
(50, '7', '43364731', 'D3-HUBUNGAN MASYARAKAT'),
(51, '7', '43253631', 'D3-PERTANAHAN'),
(52, '7', '43142531', 'D3-KEUANGAN DAERAH'),
(53, '7', '43031431', 'D3-PEMASARAN'),
(54, '7', '43420331', 'D3-ADMINISTRASI PERKANTORAN'),
(55, '5', '43494111', 'D3-MANAJEMEN USAHA PETERNAKAN'),
(56, '9', '43154411', 'D3-INST. DAN ELEKTRONIKA'),
(57, '3', '43024311', 'D3-TEKNIK SIPIL'),
(58, '3', '43034711', 'D3-DESAIN ARSITEKTUR'),
(59, '3', '43044411', 'D3-TEKNIK KIMIA'),
(60, '3', '43164111', 'D3-TEKNIK PERENC. WIL. & KOTA'),
(61, '3', '43064511', 'D3-TEKNIK MESIN'),
(62, '3', '43104511', 'D3-TEKNIK ELEKTRO'),
(63, '3', '43444211', 'D3-TEKNIK PERKAPALAN'),
(77, '1', '43314134', 'S1-ILMU HUKUM'),
(85, '7', '43364734', 'S1-ILMU KOMUNIKASI'),
(116, '3', '43164113', 'S1-TEKNIK PERENC. WIL. & KOTA'),
(133, '6', '43514232', 'S1-BAHASA JEPANG'),
(140, '7', '43374732', 'S1-HUBUNGAN INTERNASIONAL'),
(141, '5', '43494122', 'S1-TEKNOLOGI PANGAN'),
(142, '5', '43494132', 'S1-AGROEKOTEKNOLOGI'),
(144, '5', '43494142', 'S1-AGRIBISNIS'),
(150, '2', '43384632', 'S1-EKONOMI ISLAM'),
(151, '6', '43374632', 'S1-ANTROPOLOGI SOSIAL'),
(152, '6', '79404', 'D3-BAHASA JEPANG'),
(153, '5', '54101', 'S2-AGRIBISNIS'),
(154, '5', '54131', 'S2-ILMU TERNAK'),
(155, '5', '54031', 'S3-ILMU PETERNAKAN'),
(156, '4', '3001', 'S3-Ilmu Kedokteran/Kesehatan'),
(157, '1', '74102', 'S2-KENOTARIATAN'),
(158, '8', '13101', 'S2-ILMU KESEHATAN MASYARAKAT'),
(159, '8', '13151', 'S2-KESEHATAN LINGKUNGAN'),
(160, '8', '13131', 'S2-PROMOSI KESEHATAN'),
(161, '8', '13001 ', 'S3-ILMU KESEHATAN MASYARAKAT');

-- --------------------------------------------------------

--
-- Table structure for table `peminat`
--

CREATE TABLE `peminat` (
  `id` int(11) NOT NULL,
  `id_prodi` int(11) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `dt_snmptn` int(11) NOT NULL,
  `dt_sbmptn` int(11) NOT NULL,
  `dt_um` int(11) NOT NULL,
  `an_snmptn` int(11) NOT NULL,
  `an_sbmptn` int(11) NOT NULL,
  `an_um` int(11) NOT NULL,
  `te_snmptn` int(11) NOT NULL,
  `te_sbmptn` int(11) NOT NULL,
  `te_um` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminat`
--

INSERT INTO `peminat` (`id`, `id_prodi`, `tahun`, `dt_snmptn`, `dt_sbmptn`, `dt_um`, `an_snmptn`, `an_sbmptn`, `an_um`, `te_snmptn`, `te_sbmptn`, `te_um`) VALUES
(2, 47, '2015', 15, 15, 15, 30, 30, 30, 10, 13, 13);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`) VALUES
(1, 'Adib Adipraditya', 'adib', '5f4dcc3b5aa765d61d8327deb882cf99');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accreditations`
--
ALTER TABLE `accreditations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alumni_profiles`
--
ALTER TABLE `alumni_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `curiculums`
--
ALTER TABLE `curiculums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `curiculum_alumni`
--
ALTER TABLE `curiculum_alumni`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fakultas`
--
ALTER TABLE `fakultas`
  ADD PRIMARY KEY (`id_fakultas`);

--
-- Indexes for table `fees`
--
ALTER TABLE `fees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `peminat`
--
ALTER TABLE `peminat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accreditations`
--
ALTER TABLE `accreditations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `alumni_profiles`
--
ALTER TABLE `alumni_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `curiculums`
--
ALTER TABLE `curiculums`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `curiculum_alumni`
--
ALTER TABLE `curiculum_alumni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `fakultas`
--
ALTER TABLE `fakultas`
  MODIFY `id_fakultas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `fees`
--
ALTER TABLE `fees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id_jurusan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;
--
-- AUTO_INCREMENT for table `peminat`
--
ALTER TABLE `peminat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
