<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'MainController';
$route['news'] = 'MainController/news';
$route['event'] = 'MainController/event';
$route['tes/(:any)/(:any)/abc'] = 'MainController/tes/$1/$2';

//Admin
$route['admin'] = 'Admin/AdminMainController';
$route['admin/auth'] = 'Admin/AdminMainController/auth';
$route['admin/auth/login'] = 'Admin/AdminMainController/login';
$route['admin/auth/logout'] = 'Admin/AdminMainController/logout';

/*kurikulum*/
$route['admin/curiculum'] = 'Admin/CuriculumController';
$route['admin/curiculum/create'] = 'Admin/CuriculumController/create';
$route['admin/curiculum/store'] = 'Admin/CuriculumController/store';
$route['admin/curiculum/(:num)'] = 'Admin/CuriculumController/edit/$1';
$route['admin/curiculum/(:num)/update'] = 'Admin/CuriculumController/update/$1';
$route['admin/curiculum/(:num)/delete'] = 'Admin/CuriculumController/delete/$1';

/*Profil Lulusan*/
$route['admin/alumni'] = 'Admin/AlumniController';
$route['admin/alumni/create'] = 'Admin/AlumniController/create';
$route['admin/alumni/store'] = 'Admin/AlumniController/store';
$route['admin/alumni/(:num)'] = 'Admin/AlumniController/edit/$1';
$route['admin/alumni/(:num)/update'] = 'Admin/AlumniController/update/$1';
$route['admin/alumni/(:num)/delete'] = 'Admin/AlumniController/delete/$1';

/*Accreditation*/
$route['admin/accreditation'] = 'Admin/AccreditationController';
$route['admin/accreditation/create'] = 'Admin/AccreditationController/create';
$route['admin/accreditation/store'] = 'Admin/AccreditationController/store';
$route['admin/accreditation/(:num)'] = 'Admin/AccreditationController/edit/$1';
$route['admin/accreditation/(:num)/update'] = 'Admin/AccreditationController/update/$1';
$route['admin/accreditation/(:num)/delete'] = 'Admin/AccreditationController/delete/$1';

/*Fees*/
$route['admin/fee'] = 'Admin/FeeController';
$route['admin/fee/create'] = 'Admin/FeeController/create';
$route['admin/fee/store'] = 'Admin/FeeController/store';
$route['admin/fee/(:num)'] = 'Admin/FeeController/edit/$1';
$route['admin/fee/(:num)/update'] = 'Admin/FeeController/update/$1';
$route['admin/fee/(:num)/delete'] = 'Admin/FeeController/delete/$1';

/*Animo*/
$route['admin/animo'] = 'Admin/AnimoController';
$route['admin/animo/create'] = 'Admin/AnimoController/create';
$route['admin/animo/store'] = 'Admin/AnimoController/store';
$route['admin/animo/(:num)'] = 'Admin/AnimoController/edit/$1';
$route['admin/animo/(:num)/update'] = 'Admin/AnimoController/update/$1';
$route['admin/animo/(:num)/delete'] = 'Admin/AnimoController/delete/$1';

//Adminaa
$route['admin/culinary'] = 'Admin/CulinaryController';
$route['admin/culinary/create'] = 'Admin/CulinaryController/create';
$route['admin/culinary/store'] = 'Admin/CulinaryController/store';
$route['admin/culinary/(:num)'] = 'Admin/CulinaryController/edit/$1';
$route['admin/culinary/(:num)/update'] = 'Admin/CulinaryController/update/$1';
$route['admin/culinary/(:num)/delete'] = 'Admin/CulinaryController/delete/$1';

$route['admin/tourism'] = 'Admin/TourismController';
$route['admin/tourism/create'] = 'Admin/TourismController/create';
$route['admin/tourism/store'] = 'Admin/TourismController/store';
$route['admin/tourism/(:num)'] = 'Admin/TourismController/edit/$1';
$route['admin/tourism/(:num)/update'] = 'Admin/TourismController/update/$1';
$route['admin/tourism/(:num)/delete'] = 'Admin/TourismController/delete/$1';

$route['admin/event'] = 'Admin/EventController';
$route['admin/event/create'] = 'Admin/EventController/create';
$route['admin/event/store'] = 'Admin/EventController/store';
$route['admin/event/(:num)'] = 'Admin/EventController/edit/$1';
$route['admin/event/(:num)/update'] = 'Admin/EventController/update/$1';
$route['admin/event/(:num)/delete'] = 'Admin/EventController/delete/$1';

$route['admin/news'] = 'Admin/NewsController';
$route['admin/news/create'] = 'Admin/NewsController/create';
$route['admin/news/store'] = 'Admin/NewsController/store';
$route['admin/news/(:num)'] = 'Admin/NewsController/edit/$1';
$route['admin/news/(:num)/update'] = 'Admin/NewsController/update/$1';
$route['admin/news/(:num)/delete'] = 'Admin/NewsController/delete/$1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
