<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MainController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->helper('myhelper');
		$this->load->model('AllModels','',TRUE);
		
		$this->load->library('maintheme');
	} 
	 
	public function index()
	{
		$this->db->select('nama_fakultas');
		$this->db->from('fakultas');
		$query = $this->db->get();
		
		$data['fakultas'] = $query->result();
		$this->maintheme->display('main/home', $data);
	}

	public function News(){
		echo "News";
	}

	public function Event(){
		echo "Event";
	}

	public function tes($a = 0, $b = 1){
		echo $a."  ".$b;
	}
}
