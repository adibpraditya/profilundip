<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CulinaryController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->helper('myhelper');
		if(!$this->session->userdata('username'))
			redirect('/admin/auth', 'refresh');
		//$this->load->library('admintheme');
	}

	public function index()
	{
		$query = $this->db->get('culinaries');
		$data['culinaries'] = $query->result(); 
		$data['active'] = 'list';
		$this->admintheme->display('admin/culinary_list','admin/culinary_sidebar', $data);
	}

	public function create()
	{
		$data['culinaries'] = array(); 
		$data['active'] = 'create';
		$this->admintheme->display('admin/culinary_create','admin/culinary_sidebar', $data);
	}

	public function store()
	{
		if($_FILES['image']['name'] != null){
			$image_name = random10().".jpg";
			//$photoName = date("d-m-y-H-i-s").".jpg";
			$config['upload_path'] = "./upload/culinary";
			$config['allowed_types'] = 'jpg||jpeg';
			$config['max_size'] = '2048'; //on KB
			$config['file_name'] = $image_name;
			$this->load->library('upload',$config);
			//$config['file_name'];
			if($this->upload->do_upload('image')){
				
				$data = array(
					'title' => htmlentities($this->input->post('title')),
					'slug' => slugify(htmlentities($this->input->post('title'))),
					'description' => htmlentities($this->input->post('description')),
					'location' => htmlentities($this->input->post('location')),
					'position' => htmlentities($this->input->post('position')),
					'image' => $image_name
				);

				$insert = $this->db->insert('culinaries', $data);
				
				if($insert)
					$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">Culinary succes added.</div>');
				else
					$this->session->set_flashdata('msg', '<div class="alert alert-danger" role="alert">Culinary failed added.</div>');
							
				redirect('admin/culinary/create','refresh');
			}else{
				$this->session->set_flashdata('msg', '<div class="alert alert-danger" role="alert">add failed, because <b>'.$this->upload->display_errors().'</b></div>');
				redirect('admin/culinary/create','refresh');
			}
			//redirect('admin/news/add','refresh');
		}else{
			redirect('admin/culinary/create','refresh');
		}
	}	

	public function edit($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('culinaries');
		$result = $query->row();
		if($result == NULL){
			$this->load->view('errors/404ku');
		}
		else{
			$data['culinary'] = $result;
			$data['active'] = 'list';
			$this->admintheme->display('admin/culinary_edit','admin/culinary_sidebar', $data);	
		}
		
	}

	public function update($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('culinaries');
		$culinary = $query->row();
		if($culinary == NULL){
			$this->load->view('errors/404ku');
		}
		else{
			//var_dump($culinary);
			$data = array(
					'title' => htmlentities($this->input->post('title')),
					'slug' => slugify(htmlentities($this->input->post('title'))),
					'description' => htmlentities($this->input->post('description')),
					'location' => htmlentities($this->input->post('location')),
					'position' => htmlentities($this->input->post('position')),
				);

			if($_FILES['image']['name'] != null){
				$image_name = random10().".jpg";
				//$photoName = date("d-m-y-H-i-s").".jpg";
				$config['upload_path'] = "./upload/culinary";
				$config['allowed_types'] = 'jpg||jpeg';
				$config['max_size'] = '2048'; //on KB
				$config['file_name'] = $image_name;
				$this->load->library('upload',$config);
				//$config['file_name'];
				if($this->upload->do_upload('image')){
					$image = $query->row()->image;
					if($image != ''){
						unlink('upload/culinary/'.$image);	
					}
					$data['image'] = $image_name;
				}
			
			}

			$this->db->where('id', $id);
			$update = $this->db->update('culinaries', $data);
				
				if($update)
					$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">Culinary succes updated.</div>');
				else
					$this->session->set_flashdata('msg', '<div class="alert alert-danger" role="alert">Culinary failed.</div>');
			
			
			redirect('admin/culinary/'.$id, 'refresh');
		}
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('culinaries');
		$result = $query->row();
		if($result == NULL){
			$this->load->view('errors/404ku');
		}
		else{
			$image = $query->row()->image;

			$this->db->where('id', $id);
			$delete = $this->db->delete('culinaries');
			if($delete){
				if($image != ''){
					unlink('upload/culinary/'.$image);	
				}	
				$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">Culinary succes deleted.</div>');	
			}
			
			redirect('admin/culinary', 'refresh');
		}
	}
}
