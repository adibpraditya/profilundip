<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AnimoController extends CI_Controller {

	
	function __construct(){
		parent::__construct();
		$this->load->helper('myhelper');
		$this->load->model('AllModels','',TRUE);
		if(!$this->session->userdata('username'))
			redirect('/admin/auth', 'refresh');
		//$this->load->library('admintheme');
	}

	public function index()
	{
		$this->db->select('peminat.*,jurusan.nama_jurusan as department, fakultas.nama_fakultas as faculty');
		$this->db->from('peminat');
		$this->db->join('jurusan', 'peminat.id_prodi = jurusan.id_jurusan');
		$this->db->join('fakultas', 'jurusan.id_fakultas = fakultas.id_fakultas');
		$this->db->order_by('nama_fakultas', 'ASC');
		$this->db->order_by('nama_jurusan', 'ASC');
		$query = $this->db->get();
		$data['peminat'] = $query->result(); 
		$data['active'] = 'list';
		$this->admintheme->display('admin/animo_list','admin/animo_sidebar', $data);
	}

	public function create()
	{
		$data['peminat'] = array(); 
		$data['faculties'] = $this->AllModels->getFaculties();
		$data['departments'] = $this->AllModels->getDepartments();
		$data['active'] = 'create';
		$this->admintheme->display('admin/animo_create','admin/animo_sidebar', $data);
	}

	public function store()
	{
		$data = array(
			'id_prodi' => htmlentities($this->input->post('prodi')),
			'tahun' => htmlentities($this->input->post('tahun')),
			'dt_snmptn' => htmlentities($this->input->post('dt_snmptn')),
			'dt_sbmptn' => htmlentities($this->input->post('dt_sbmptn')),
			'dt_um' => htmlentities($this->input->post('dt_um')),
			'an_snmptn' => htmlentities($this->input->post('an_snmptn')),
			'an_sbmptn' => htmlentities($this->input->post('an_sbmptn')),
			'an_um' => htmlentities($this->input->post('an_um')),
			'te_snmptn' => htmlentities($this->input->post('te_snmptn')),
			'te_sbmptn' => htmlentities($this->input->post('te_sbmptn')),
			'te_um' => htmlentities($this->input->post('te_um'))
		);
		
		$insert = $this->db->insert('peminat', $data);

		if($insert){
			$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">peminat succes added.</div>');
			redirect('admin/animo/', 'refresh');
		}
		else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger" role="alert">peminat failed.</div>');
			redirect('admin/animo/create/', 'refresh');
		}
	}	

	public function edit($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('peminat');
		$result = $query->row();
		$data['departments'] = $this->AllModels->getDepartments();

		if($result == NULL){
			//echo "a"; die;
			$this->load->view('errors/404ku');
		}
		else{
			//echo "a"; die;
			$data['peminat'] = $result;
			$data['active'] = 'list';
			$this->admintheme->display('admin/animo_edit','admin/animo_sidebar', $data);	
		}
		
	}

	public function update($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('peminat');
		$curiculum = $query->row();
		if($curiculum == NULL){
			$this->load->view('errors/404ku');
		}
		else{
			//var_dump($curiculum);
			$data = array(
				'id_prodi' => htmlentities($this->input->post('prodi')),
				'tahun' => htmlentities($this->input->post('tahun')),
				'dt_snmptn' => htmlentities($this->input->post('dt_snmptn')),
				'dt_sbmptn' => htmlentities($this->input->post('dt_sbmptn')),
				'dt_um' => htmlentities($this->input->post('dt_um')),
				'an_snmptn' => htmlentities($this->input->post('an_snmptn')),
				'an_sbmptn' => htmlentities($this->input->post('an_sbmptn')),
				'an_um' => htmlentities($this->input->post('an_um')),
				'te_snmptn' => htmlentities($this->input->post('te_snmptn')),
				'te_sbmptn' => htmlentities($this->input->post('te_sbmptn')),
				'te_um' => htmlentities($this->input->post('te_um'))
			);

			$this->db->where('id', $id);
			$update = $this->db->update('peminat', $data);
				
				if($update)
					$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">Data Peminat succes updated.</div>');
				else
					$this->session->set_flashdata('msg', '<div class="alert alert-danger" role="alert">Data Peminat failed.</div>');
			
			
			redirect('admin/animo/'.$id, 'refresh');
		}
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('peminat');
		$result = $query->row();
		if($result == NULL){
			$this->load->view('errors/404ku');
		}
		else{
			$this->db->where('id', $id);
			$delete = $this->db->delete('peminat');
			if($delete){
				$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">Data Peminat succes deleted.</div>');	
			}
			
			redirect('admin/animo', 'refresh');
		}
	}
}
