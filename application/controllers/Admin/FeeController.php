<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FeeController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->helper('myhelper');
		$this->load->model('AllModels','',TRUE);
		if(!$this->session->userdata('username'))
			redirect('/admin/auth', 'refresh');
		//$this->load->library('admintheme');
	}

	public function index()
	{
		$this->db->select('fees.*,jurusan.nama_jurusan as department, fakultas.nama_fakultas as faculty');
		$this->db->from('fees');
		$this->db->join('jurusan', 'fees.id_prodi = jurusan.id_jurusan');
		$this->db->join('fakultas', 'jurusan.id_fakultas = fakultas.id_fakultas');
		$this->db->group_by('fees.id_prodi');
		$this->db->order_by('nama_fakultas', 'ASC');
		$this->db->order_by('nama_jurusan', 'ASC');
		$query = $this->db->get();
		$data['fees'] = $query->result(); 
		$data['active'] = 'list';
		$this->admintheme->display('admin/fee_list','admin/fee_sidebar', $data);
	}

	public function create()
	{
		$data['fees'] = array(); 
		$data['faculties'] = $this->AllModels->getFaculties();
		$data['departments'] = $this->AllModels->getDepartments();
		$data['active'] = 'create';
		$this->admintheme->display('admin/fee_create','admin/fee_sidebar', $data);
	}

	public function store()
	{
		$fees = json_decode($this->input->post('fees'));

		foreach ($fees as $key => $fee) {
		
			$data = array(
				'id_prodi' => htmlentities($this->input->post('prodi')),
				'jalur' => htmlentities($fee->jalur),
				'ukt_1' => htmlentities($fee->ukt1),
				'ukt_2' => htmlentities($fee->ukt2),
				'ukt_3' => htmlentities($fee->ukt3),
				'ukt_4' => htmlentities($fee->ukt4),
				'ukt_5' => htmlentities($fee->ukt5),
				'ukt_6' => htmlentities($fee->ukt6),
				'ukt_7' => htmlentities($fee->ukt7),
				'spi_1' => htmlentities($fee->spi1),
				'spi_2' => htmlentities($fee->spi2),
			);
		
			$insert = $this->db->insert('fees', $data);
		}


		//if($insert){
		$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">Biya Kuliah succes added.</div>');
		redirect('admin/fee/', 'refresh');
		/*}
		else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger" role="alert">Biaya Kuliah failed.</div>');
			//redirect('admin/create/', 'refresh');
		}*/
	}	

	public function edit($id)
	{
		$this->db->where('id_prodi', $id);
		$query = $this->db->get('fees');
		$fees = $query->result();
		$data['departments'] = $this->AllModels->getDepartments();
		if($fees == NULL){
			$this->load->view('errors/404ku');
		}
		else{
			$fees_array = array();
			foreach ($fees as $key => $fee) {
				$fees_array[] = array(
					'jalur' => $fee->jalur,
					'ukt1' => $fee->ukt_1,
					'ukt2' => $fee->ukt_2,
					'ukt3' => $fee->ukt_3,
					'ukt4' => $fee->ukt_4,
					'ukt5' => $fee->ukt_5,
					'ukt6' => $fee->ukt_6,
					'ukt7' => $fee->ukt_7,
					'spi1' => $fee->spi_1,
					'spi2' => $fee->spi_2
				);
			}
			$data['fees_json'] = json_encode($fees_array);
			//echo $data['fees_json'];die;
			$data['id_prodi'] = $id;
			$data['active'] = 'list';
			$this->admintheme->display('admin/fee_edit','admin/fee_sidebar', $data);	
		}
		
	}

	public function update($id)
	{
		$this->db->where('id_prodi', $id);
		$query = $this->db->get('fees');
		$fee = $query->result();
		if($fee == NULL){
			$this->load->view('errors/404ku');
		}
		else{
			//var_dump($fee);
			$this->db->where('id_prodi', $id);
			$this->db->delete('fees');
			$fees = json_decode($this->input->post('fees'));

			foreach ($fees as $key => $fee) {
			
				$data = array(
					'id_prodi' => htmlentities($this->input->post('prodi')),
					'jalur' => htmlentities($fee->jalur),
					'ukt_1' => htmlentities($fee->ukt1),
					'ukt_2' => htmlentities($fee->ukt2),
					'ukt_3' => htmlentities($fee->ukt3),
					'ukt_4' => htmlentities($fee->ukt4),
					'ukt_5' => htmlentities($fee->ukt5),
					'ukt_6' => htmlentities($fee->ukt6),
					'ukt_7' => htmlentities($fee->ukt7),
					'spi_1' => htmlentities($fee->spi1),
					'spi_2' => htmlentities($fee->spi2),
				);
			
				$insert = $this->db->insert('fees', $data);
			}
				
			
			$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">Biaya Kuliah succes updated.</div>');
			
			redirect('admin/fee/'.$id, 'refresh');
		}
	}

	public function delete($id)
	{
		$this->db->where('id_prodi', $id);
		$query = $this->db->get('fees');
		$result = $query->result();
		if($result == NULL){
			$this->load->view('errors/404ku');
		}
		else{
			$this->db->where('id_prodi', $id);
			$delete = $this->db->delete('fees');
			if($delete){
				$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">Biaya Kuliah succes deleted.</div>');	
			}
			
			redirect('admin/fee', 'refresh');
		}
	}
}
