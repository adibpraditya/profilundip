<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminMainController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if(!$this->session->userdata('username'))
			redirect('/admin/auth', 'refresh');
		$data = array();
		$this->admintheme->display('admin/admin_home','admin/admin_sidebar', $data);
	}

	public function auth(){
		if($this->session->userdata('username'))
			redirect('/admin', 'refresh');

		$data = array();
		$this->load->view('admin/login', $data);
	}

	public function login(){
		if($this->session->userdata('username'))
			redirect('/admin', 'refresh');

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$this->db->where('username', $username);
		$this->db->where('password', md5($password));
		$user = $this->db->get('users')->row();

		//var_dump($user); die;

		if($user != NULL){
			$sessiondata = array(
				'username' => $user->username,
				'name' => $user->name
			);
			$this->session->set_userdata($sessiondata);
			redirect('admin');
		} else {
			$this->session->set_flashdata('error_login', '<div class="alert alert-danger" role="alert">Login Failed. Wrong Username or Password.</div>');
			redirect('admin/auth');
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		//$this->session->set_flashdata('logout', 'Anda telah keluar dari sistem');
		redirect('admin/auth');
	}

}
