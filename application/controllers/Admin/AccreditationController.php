<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AccreditationController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->helper('myhelper');
		$this->load->model('AllModels','',TRUE);
		if(!$this->session->userdata('username'))
			redirect('/admin/auth', 'refresh');
		//$this->load->library('admintheme');
	}

	public function index()
	{
		$this->db->select('accreditations.*,jurusan.nama_jurusan as department, fakultas.nama_fakultas as faculty');
		$this->db->from('accreditations');
		$this->db->join('jurusan', 'accreditations.id_prodi = jurusan.id_jurusan');
		$this->db->join('fakultas', 'jurusan.id_fakultas = fakultas.id_fakultas');
		$this->db->order_by('nama_fakultas', 'ASC');
		$this->db->order_by('nama_jurusan', 'ASC');
		$query = $this->db->get();
		$data['accreditations'] = $query->result(); 
		$data['active'] = 'list';
		$this->admintheme->display('admin/accreditation_list','admin/accreditation_sidebar', $data);
	}

	public function create()
	{
		$data['accreditations'] = array(); 
		$data['active'] = 'create';
		$data['departments'] = $this->AllModels->getDepartments();
		$this->admintheme->display('admin/accreditation_create','admin/accreditation_sidebar', $data);
	}

	public function store()
	{
		if($_FILES['image']['name'] != null){
			$image_name = random10().".jpg";
			//$photoName = date("d-m-y-H-i-s").".jpg";
			$config['upload_path'] = "./upload/accreditation";
			$config['allowed_types'] = 'jpg||jpeg';
			$config['max_size'] = '2048'; //on KB
			$config['file_name'] = $image_name;
			$this->load->library('upload',$config);
			//$config['file_name'];
			if($this->upload->do_upload('image')){
				
				$data = array(
					'id_prodi' => htmlentities($this->input->post('prodi')),
					'sk_number' => htmlentities($this->input->post('sk')),
					'accreditation' => htmlentities($this->input->post('accreditation')),
					'date_end' => htmlentities($this->input->post('date_end')),
					'image' => $image_name
				);

				$insert = $this->db->insert('accreditations', $data);
				
				if($insert)
					$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">accreditation succes added.</div>');
				else
					$this->session->set_flashdata('msg', '<div class="alert alert-danger" role="alert">accreditation failed added.</div>');
							
				redirect('admin/accreditation/create','refresh');
			}else{
				$this->session->set_flashdata('msg', '<div class="alert alert-danger" role="alert">add failed, because <b>'.$this->upload->display_errors().'</b></div>');
				redirect('admin/accreditation/create','refresh');
			}
			//redirect('admin/news/add','refresh');
		}else{
			redirect('admin/accreditation/create','refresh');
		}
	}	

	public function edit($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('accreditations');
		$result = $query->row();
		if($result == NULL){
			$this->load->view('errors/404ku');
		}
		else{
			$data['accreditation'] = $result;
			$data['active'] = 'list';
			$data['departments'] = $this->AllModels->getDepartments();
			$this->admintheme->display('admin/accreditation_edit','admin/accreditation_sidebar', $data);	
		}
		
	}

	public function update($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('accreditations');
		$accreditation = $query->row();
		if($accreditation == NULL){
			$this->load->view('errors/404ku');
		}
		else{
			//var_dump($accreditation);
			$data = array(
					'id_prodi' => htmlentities($this->input->post('prodi')),
					'sk_number' => htmlentities($this->input->post('sk')),
					'accreditation' => htmlentities($this->input->post('accreditation')),
					'date_end' => htmlentities($this->input->post('date_end'))
				);

			if($_FILES['image']['name'] != null){
				$image_name = random10().".jpg";
				//$photoName = date("d-m-y-H-i-s").".jpg";
				$config['upload_path'] = "./upload/accreditation";
				$config['allowed_types'] = 'jpg||jpeg';
				$config['max_size'] = '2048'; //on KB
				$config['file_name'] = $image_name;
				$this->load->library('upload',$config);
				//$config['file_name'];
				if($this->upload->do_upload('image')){
					$image = $query->row()->image;
					if($image != ''){
						unlink('upload/accreditation/'.$image);	
					}
					$data['image'] = $image_name;
				}
			
			}

			$this->db->where('id', $id);
			$update = $this->db->update('accreditations', $data);
				
				if($update)
					$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">accreditation succes updated.</div>');
				else
					$this->session->set_flashdata('msg', '<div class="alert alert-danger" role="alert">accreditation failed.</div>');
			
			
			redirect('admin/accreditation/'.$id, 'refresh');
		}
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('accreditations');
		$result = $query->row();
		if($result == NULL){
			$this->load->view('errors/404ku');
		}
		else{
			$image = $query->row()->image;

			$this->db->where('id', $id);
			$delete = $this->db->delete('accreditations');
			if($delete){
				if($image != ''){
					unlink('upload/accreditation/'.$image);	
				}	
				$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">accreditation succes deleted.</div>');	
			}
			
			redirect('admin/accreditation', 'refresh');
		}
	}
}
