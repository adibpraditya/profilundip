<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AlumniController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->helper('myhelper');
		if(!$this->session->userdata('username'))
			redirect('/admin/auth', 'refresh');
		//$this->load->library('admintheme');
	}

	public function index()
	{
		$query = $this->db->get('alumni_profiles');
		$data['profiles'] = $query->result(); 
		$data['active'] = 'list';
		$data['form'] = base_url().'admin/alumni/store';
		$this->admintheme->display('admin/alumni_profile','admin/alumni_profile_sidebar', $data);
	}

	public function create()
	{
		$data['curiculums'] = array(); 
		$data['faculties'] = $this->AllModels->getFaculties();
		$data['departments'] = $this->AllModels->getDepartments();
		$data['active'] = 'create';
		$this->admintheme->display('admin/curiculum_create','admin/curiculum_sidebar', $data);
	}

	public function store()
	{
		//echo "a"; die;
		$data = array(
			'profesi' => htmlentities($this->input->post('profesi'))
		);
		
		$insert = $this->db->insert('alumni_profiles', $data);

		if($insert)
			$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">Profil Lulusan success added.</div>');
		else
			$this->session->set_flashdata('msg', '<div class="alert alert-danger" role="alert">Profil Lulusan added failed.</div>');
			
		redirect('admin/alumni/', 'refresh');
		
	}	

	public function edit($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('alumni_profiles');
		$result = $query->row();
		if($result == NULL){
			$this->load->view('errors/404ku');
		}
		else{
			$data['profile'] = $result;
			$data['active'] = 'list';
			$data['form'] = base_url().'admin/alumni/'.$result->id.'/update';
			$query = $this->db->get('alumni_profiles');
			$data['profiles'] = $query->result();
			$this->admintheme->display('admin/alumni_profile','admin/alumni_profile_sidebar', $data);
		}
		
	}

	public function update($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('alumni_profiles');
		$curiculum = $query->row();
		if($curiculum == NULL){
			$this->load->view('errors/404ku');
		}
		else{
			//var_dump($curiculum);
			$data = array(
				'profesi' => htmlentities($this->input->post('profesi'))
			);

			$this->db->where('id', $id);
			$update = $this->db->update('alumni_profiles', $data);
				
				if($update)
					$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">Profil Lulusan succes updated.</div>');
				else
					$this->session->set_flashdata('msg', '<div class="alert alert-danger" role="alert">Profil Lulusan failed updated.</div>');
			
			
			redirect('admin/alumni/'.$id, 'refresh');
		}
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('alumni_profiles');
		$result = $query->row();
		if($result == NULL){
			$this->load->view('errors/404ku');
		}
		else{
			$this->db->where('id', $id);
			$delete = $this->db->delete('alumni_profiles');
			if($delete){
				$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">Profil Lulusan succes deleted.</div>');	
			}
			
			redirect('admin/alumni', 'refresh');
		}
	}
}
