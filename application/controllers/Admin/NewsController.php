<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NewsController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		echo "Ini index news";
	}

	public function create()
	{
		echo "Ini create new news ";
	}

	public function store()
	{
		echo "Ini store new news ";
	}	

	public function edit($id)
	{
		echo "Ini news ".$id;
	}

	public function update($id)
	{
		echo "Ini update news ".$id;
	}

	public function delete($id)
	{
		echo "Ini delete news ".$id;
	}
}
