<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CuriculumController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->helper('myhelper');
		$this->load->model('AllModels','',TRUE);
		$this->load->model('CuriculumModel','',TRUE);
		if(!$this->session->userdata('username'))
			redirect('/admin/auth', 'refresh');
		//$this->load->library('admintheme');
	}

	public function index()
	{
		$this->db->select('curiculums.*,jurusan.nama_jurusan as department, fakultas.nama_fakultas as faculty');
		$this->db->from('curiculums');
		$this->db->join('jurusan', 'curiculums.id_prodi = jurusan.id_jurusan');
		$this->db->join('fakultas', 'jurusan.id_fakultas = fakultas.id_fakultas');
		$this->db->order_by('nama_fakultas', 'ASC');
		$this->db->order_by('nama_jurusan', 'ASC');
		$query = $this->db->get();
		$data['curiculums'] = $query->result(); 
		$data['active'] = 'list';
		$this->admintheme->display('admin/curiculum_list','admin/curiculum_sidebar', $data);
	}

	public function create()
	{
		//var_dump($this->CuriculumModel->getNextId());die;
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->ckeditor->basePath = base_url().'assets/js/ckeditor/';
		$this->ckeditor->config['toolbar'] = "Full";
		$this->ckeditor->config['language'] = 'en';
		
		//Add Ckfinder to Ckeditor
		$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets/js/ckfinder/');
	
		$data['curiculums'] = array(); 
		$data['faculties'] = $this->AllModels->getFaculties();
		$data['departments'] = $this->AllModels->getDepartments();
		$data['profiles'] = $this->AllModels->getAlumniProfiles();
		$data['active'] = 'create';
		$this->admintheme->display('admin/curiculum_create','admin/curiculum_sidebar', $data);
	}

	public function store()
	{
		$prof_json = $this->input->post('profesi');
		//var_dump($prof);die;
		$data = array(
			'id_prodi' => htmlentities($this->input->post('prodi')),
			'materi' => htmlentities($this->input->post('materi')),
			'izin' => htmlentities($this->input->post('izin')),
			'gelar' => htmlentities($this->input->post('gelar')),
			'kkni' => htmlentities($this->input->post('kkni')),
			'tujuan' => $this->input->post('tujuan'),
			'kompetensi' => $this->input->post('kompetensi'),
			'pembelajaran' => $this->input->post('pembelajaran'),
			'persyaratan' => $this->input->post('persyaratan'),
			'dukungan' => $this->input->post('dukungan'),
			'syarat_admisi' => $this->input->post('syarat_admisi'),
			'lain_lain' => $this->input->post('lain_lain'),
			'video' => htmlentities($this->input->post('video')),
		);
		

		$insert = $this->db->insert('curiculums', $data);
		//die;
		if($insert){

			$id_now = $this->CuriculumModel->getNextId();
			$profesi = json_decode($prof_json);
			//var_dump($profesi);
			foreach($profesi as $prof){
				$curiculum_alumni[] = array(
						'id_curiculum' => $id_now,
						'id_alumni_profile' => $prof->id_prof
					);
			}

			if(isset($curiculum_alumni))
				$insert_profesi = $this->db->insert_batch('curiculum_alumni', $curiculum_alumni);
			//var_dump($curiculum_alumni);

			$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">curiculum succes added.</div>');
			redirect('admin/curiculum/', 'refresh');
		}
		else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger" role="alert">curiculum failed.</div>');
			redirect('admin/create/', 'refresh');
		}
	}	

	public function edit($id)
	{
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->ckeditor->basePath = base_url().'assets/js/ckeditor/';
		$this->ckeditor->config['toolbar'] = "Full";
		$this->ckeditor->config['language'] = 'en';
		
		//Add Ckfinder to Ckeditor
		$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets/js/ckfinder/');
	
		$this->db->where('id', $id);
		$query = $this->db->get('curiculums');
		$result = $query->row();
		$data['departments'] = $this->AllModels->getDepartments();
		if($result == NULL){
			$this->load->view('errors/404ku');
		}
		else{
			$data['curiculum'] = $result;
			$data['active'] = 'list';
			$data['profiles'] = $this->AllModels->getAlumniProfiles();
			$this->admintheme->display('admin/curiculum_edit','admin/curiculum_sidebar', $data);	
		}
	}

	public function update($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('curiculums');
		$curiculum = $query->row();
		if($curiculum == NULL){
			$this->load->view('errors/404ku');
		}
		else{
			//var_dump($curiculum);
			$data = array(
					'id_prodi' => htmlentities($this->input->post('prodi')),
					'materi' => htmlentities($this->input->post('materi')),
					'izin' => htmlentities($this->input->post('izin')),
					'gelar' => htmlentities($this->input->post('gelar')),
					'kkni' => htmlentities($this->input->post('kkni')),
					'tujuan' => $this->input->post('tujuan'),
					'kompetensi' => $this->input->post('kompetensi'),
					'pembelajaran' => $this->input->post('pembelajaran'),
					'persyaratan' => $this->input->post('persyaratan'),
					'dukungan' => $this->input->post('dukungan'),
					'syarat_admisi' => $this->input->post('syarat_admisi'),
					'lain_lain' => $this->input->post('lain_lain'),
					'video' => htmlentities($this->input->post('video')),
				);

			$this->db->where('id', $id);
			$update = $this->db->update('curiculums', $data);
				
				if($update)
					$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">curiculum succes updated.</div>');
				else
					$this->session->set_flashdata('msg', '<div class="alert alert-danger" role="alert">curiculum failed.</div>');
			
			
			redirect('admin/curiculum/'.$id, 'refresh');
		}
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('curiculums');
		$result = $query->row();
		if($result == NULL){
			$this->load->view('errors/404ku');
		}
		else{
			$this->db->where('id', $id);
			$delete = $this->db->delete('curiculums');
			if($delete){
				$this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert">curiculum succes deleted.</div>');	
			}
			
			redirect('admin/curiculum', 'refresh');
		}
	}
}
