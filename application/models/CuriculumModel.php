<?php
	class CuriculumModel extends CI_Model{
		public function __Construct(){
			parent:: __Construct();
		}

		public function getProfiles($id=null){
			if($id == null)
				return null;

			$this->db->select('alumni_profiles.id, alumni_profiles.profesi');
			$this->db->from('curiculum_alumni');
			$this->db->join('curiculums', 'curiculum_alumni.id_curiculum = curiculums.id');
			$this->db->join('alumni_profiles', 'curiculum_alumni.id_alumni_profile = alumni_profiles.id');
			$this->db->where('curiculum_alumni.id_curiculum', $id);
			$this->db->order_by('curiculum_alumni.id', 'ASC');
			$query = $this->db->get();
			return $query->result();
		}

		public function getNextId(){
			$this->db->select('MAX(id) as id');
			$this->db->from('curiculums');
			$query = $this->db->get();
			return $query->row()->id;
		}
	}