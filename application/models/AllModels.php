<?php
	class AllModels extends CI_Model{
		public function __Construct(){
			parent:: __Construct();
		}
		
		public function insert($table, $data){
			return $this->db->insert($table, $data);
		}
		
		public function getFaculties(){
			$this->db->select('*');
			$this->db->from('fakultas');
			
			$query = $this->db->get();
			return $query->result();
		}
		
		public function getDepartments(){
			$this->db->select('*');
			$this->db->from('jurusan');
			$this->db->join('fakultas', 'jurusan.id_fakultas = fakultas.id_fakultas');
			$this->db->order_by('nama_fakultas', 'ASC');
			$this->db->order_by('nama_jurusan', 'ASC');
			$query = $this->db->get();
			return $query->result();
		}

		public function getAlumniProfiles(){
			$profiles = $this->db->get('alumni_profiles')->result();
			
			/*$profilesArray = array();
			foreach ($profiles as $key => $profile) {
				$profilesArray[] = [
					'id' => $profile->id,
					'profesi' => $profile->profesi
				];	
			}*/
			
			return $profiles;
		}
	}
?>