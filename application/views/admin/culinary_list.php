	<div class="content-wrapper">
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<?php
					
					if($this->session->flashdata('msg')){
						echo $this->session->flashdata('msg');
					}
					?>
					<table class="table">
						<tr>
							<th>#</th>
							<th>Title</th>
							<th>Loctaion</th>
							<th>Description</th>
							<th>Action</th>
						</tr>
						<?php
							foreach ($culinaries as $key => $cul) {
						?>
							<tr>
								<td><?php echo $key+1; ?></td>
								<td><?php echo $cul->title; ?></td>
								<td><?php echo $cul->location; ?></td>
								<td><?php echo $cul->description; ?></td>
								<td><a href="<?php echo base_url('admin/culinary/'.$cul->id); ?>" class="btn btn-primary btn-xs">Detail</a></td>
							</tr>
						<?php		
							}
						?>
					</table>
				</div>
			</div>
		</div>
	</div>
