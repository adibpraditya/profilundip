

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Profil Program Studi Admin</title>

	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
</head>
<body>
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header logo-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">
<!-- 					<img class="pull-left" src="http://en.pelangibeverage.com/image/logo-top.png" height="34" style="margin-top: -7px; padding-right: 7px">
 -->					Profil Program Studi Admin
				</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				
							</div>
		</div>
	</div>

	<div class="page-wrapper">
				<div class="content-wrapper content-wrapper-no-sidebar">
			<div class="content">
					<div class="container-fluid">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<?php
					
					if($this->session->flashdata('error_login')){
						echo $this->session->flashdata('error_login');
					}
				?>
				<div class="panel panel-default">
					<div class="panel-heading">Login</div>
					<div class="panel-body">
						
						<form method="POST" action="<?php echo base_url(); ?>admin/auth/login" accept-charset="UTF-8" role="form" class="form-horizontal"><input name="_token" type="hidden" value="8frwVIu7I6hWbvZQ0kEspB6AxHe2NeWzAV5taWCE">

						<div class="form-group">
							<label class="col-md-4 control-label">Username</label>
							<div class="col-md-6">
								<input class="form-control" name="username" type="text" value="">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input class="form-control" name="password" type="password" value="">
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">Login</button>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
			</div>
		</div>
			</div>

	<footer>
		<div class="copyright">
			Explore Admin by JagoDevelop &copy;
		</div>
	</footer>

	<!-- Modal -->
	
	<!-- Script -->
	<script src="<?php echo base_url();?>assets/js/scriptku.js"></script>
	</body>
</html>
