	<div class="content-wrapper">
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<?php
					
					if($this->session->flashdata('msg')){
						echo $this->session->flashdata('msg');
					}
					?>
					<h3>Daftar Kurikulum</h3>
					<table class="table">
						<tr>
							<th>#</th>
							<th>Fakultas</th>
							<th>Jurusan</th>
							<th>Detail</th>
						</tr>
						<?php
							foreach ($curiculums as $key => $cul) {
						?>
							<tr>
								<td><?php echo $key+1; ?></td>
								<td><?php echo $cul->faculty; ?></td>
								<td><?php echo $cul->department; ?></td>
								<td><a href="<?php echo base_url('admin/curiculum/'.$cul->id); ?>" class="btn btn-primary btn-xs">Detail</a></td>
							</tr>
						<?php		
							}
						?>
					</table>
				</div>
			</div>
		</div>
	</div>
