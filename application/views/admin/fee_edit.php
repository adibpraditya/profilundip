<div class="content-wrapper">
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="content-header">
					<h2>
						Edit Biaya Kuliah
						<?php echo form_open_multipart(base_url().'admin/fee/'.$id_prodi.'/delete'); ?>

						<button type="submit" class="btn btn-danger"
							onclick="return confirm('Anda yakin akan menghapus data biaya kuliah ini?');"
						>
							Hapus
						</button>
						<?php echo form_close(); ?>
					</h2>
				</div>
			</div>
			<div class="row">
				<?php
					
					if($this->session->flashdata('msg')){
						echo $this->session->flashdata('msg');
					}
				?>

				<?php echo form_open_multipart(base_url().'admin/fee/'.$id_prodi.'/update', array('id'=>'update')); ?>
				<div class="form-group">
					<label for="customer" class="control-label">
						Program Studi
					</label>
					<div class="control-input ">
						<select name="prodi" class="form-control" id="prodi" required>
							<option value="">-Pilih Prodi-</option>
							<?php
								foreach($departments as $department){
							?>
								<option class="<?php echo $department->id_fakultas;?>" value="<?php echo $department->id_jurusan;?>" <?php echo $department->id_jurusan == $id_prodi ? "selected" : ""; ?>>
									<?php echo $department->nama_jurusan;?>
								</option>
							<?php	
									
								}
							?>
						</select>
					</div>
				</div>

				<label for="biaya" class="control-label">
					Tabel Biaya
				</label>
				
				<table class="table table-bordered">
					<thead>
						<tr>
							<th rowspan="2">No</th>
							<th rowspan="2">Jalur</th>
							<th colspan="7">UKT</th>
							<th colspan="2">SPI</th>
							<th rowspan="2">Aksi</th>
						</tr>
						<tr>
							<th>GOL 1</th>
							<th>GOL 2</th>
							<th>GOL 3</th>
							<th>GOL 4</th>
							<th>GOL 5</th>
							<th>GOL 6</th>
							<th>GOL 7</th>
							<th>GOL 1</th>
							<th>GOL 2</th>
						</tr>
					</thead>
					<tbody id="tbody_fee">
					</tbody>
				</table>
				<input type="hidden" value='<?php echo $fees_json; ?>' id="fees" name="fees">
				
				<div class="form-group">
					<?php echo form_submit(['name'=>'kirim', 'value'=>'Simpan', 'class'=>'btn btn-primary']); ?>
				</div>
				
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url();?>assets/js/scriptku.js"></script>
<script type="x-tmpl-mustache" id="fee-table">
	<tr>
		<td>
		</td>
		<td>
			<?php echo form_input(array('name'=>'jalur', 'value'=>null, 'class'=>'form-control', 'id'=>'jalur', 'placeholder'=>'Masukkan Jalur')); ?>
		</td>
		<td>
			<?php echo form_input(array('name'=>'ukt1', 'value'=>0, 'class'=>'form-control', 'id'=>'ukt1', 'placeholder'=>'UKT 1')); ?>
		</td>
		<td>
			<?php echo form_input(array('name'=>'ukt2', 'value'=>0, 'class'=>'form-control', 'id'=>'ukt2', 'placeholder'=>'UKT 2')); ?>
		</td>
		<td>
			<?php echo form_input(array('name'=>'ukt3', 'value'=>0, 'class'=>'form-control', 'id'=>'ukt3', 'placeholder'=>'UKT 3')); ?>
		</td>
		<td>
			<?php echo form_input(array('name'=>'ukt4', 'value'=>0, 'class'=>'form-control', 'id'=>'ukt4', 'placeholder'=>'UKT 4')); ?>
		</td>
		<td>
			<?php echo form_input(array('name'=>'ukt5', 'value'=>0, 'class'=>'form-control', 'id'=>'ukt5', 'placeholder'=>'UKT 5')); ?>
		</td>
		<td>
			<?php echo form_input(array('name'=>'ukt6', 'value'=>0, 'class'=>'form-control', 'id'=>'ukt6', 'placeholder'=>'UKT 6')); ?>
		</td>
		<td>
			<?php echo form_input(array('name'=>'ukt7', 'value'=>0, 'class'=>'form-control', 'id'=>'ukt7', 'placeholder'=>'UKT 7')); ?>
		</td>
		<td>
			<?php echo form_input(array('name'=>'spi1', 'value'=>0, 'class'=>'form-control', 'id'=>'spi1', 'placeholder'=>'SPI 1')); ?>
		</td>
		<td>
			<?php echo form_input(array('name'=>'spi2', 'value'=>0, 'class'=>'form-control', 'id'=>'spi2', 'placeholder'=>'SPI 2')); ?>
		</td>
		<td>
			<button type="button" class="btn btn-info" id="tambah">Tambah</button>
		</td>
	</tr>
	{% #item %}
		<tr>
			<td>
				{% no %}
			</td>
			<td>
				{% jalur %}
			</td>
			<td>
				{% ukt1 %}
			</td>
			<td>
				{% ukt2 %}
			</td>
			<td>
				{% ukt3 %}
			</td>
			<td>
				{% ukt4 %}
			</td>
			<td>
				{% ukt5 %}
			</td>
			<td>
				{% ukt6 %}
			</td>
			<td>
				{% ukt7 %}
			</td>
			<td>
				{% spi1 %}
			</td>
			<td>
				{% spi2 %}
			</td>
			<td>
				<button type="button" id="delete_{% _id %}" class="btn btn-danger btn-xs btn-delete-item" data-id="{% _id %}">hapus</button>
			</td>
		</tr>
	{% /item %}
</script>

<script>
	Mustache.tags = ['{%', '%}'];
	var fees = JSON.parse($('#fees').val());
	//console.log(fees);

	function render(){
		getIndex();
		$('#fees').val(JSON.stringify(fees));
		//console.log(fees);
		var tmpl = $('#fee-table').html();
		//console.log(tmpl);
		Mustache.parse(tmpl);
		var html = Mustache.render(tmpl, { item : fees });
		$('#tbody_fee').html(html);

		bind();
	}

	function bind(){
		$('#tambah').on('click', tambah);
		$('.btn-delete-item').on('click', del);
	}

	function getIndex() 
	{
		for (idx in fees) {
			fees[idx]['_id'] = idx;
			fees[idx]['no'] = parseInt(idx) + 1;
		}
	}

	function tambah(){
		var jalur = $('#jalur').val();
		var ukt1 = $('#ukt1').val();
		var ukt2 = $('#ukt2').val();
		var ukt3 = $('#ukt3').val();
		var ukt4 = $('#ukt4').val();
		var ukt5 = $('#ukt5').val();
		var ukt6 = $('#ukt6').val();
		var ukt7 = $('#ukt7').val();
		var spi1 = $('#spi1').val();
		var spi2 = $('#spi2').val();

		if (!(jalur && ukt1 && ukt2 && ukt3 && ukt4 && ukt5 && ukt6 && ukt7 && spi1 && spi2)){
			alert("Data Biaya ada yang Kosong");
			return;
		}

		var input = { 
			'jalur' : jalur,
			'ukt1' : ukt1,
			'ukt2' : ukt2,
			'ukt3' : ukt3,
			'ukt4' : ukt4,
			'ukt5' : ukt5,
			'ukt6' : ukt6,
			'ukt7' : ukt7,
			'spi1' : spi1,
			'spi2' : spi2

		}
		fees.push(input);
		render();
	}

	function del() 
	{
		var i = parseInt($(this).data('id'), 10);
		
		fees.splice(i, 1);
		render();
	}
	
	function fees_validasi(){
		var fees = JSON.parse($('#kompetensi_dasar').val());
		console.log(fees.length);
		if(fees.length < 1){
			$('#fees').addClass('alert alert-danger');
			alert('Minimal Ada 1 Kompetensi Dasar');

    		return false;
		}
	}

	render();
</script>