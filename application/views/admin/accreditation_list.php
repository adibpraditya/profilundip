	<div class="content-wrapper">
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<?php
					
					if($this->session->flashdata('msg')){
						echo $this->session->flashdata('msg');
					}
					?>
					<h3>Daftar Akreditasi</h3>
					<table class="table">
						<tr>
							<th>#</th>
							<th>Program Studi</th>
							<th>No. SK Akreditasi</th>
							<th>Akreditasi</th>
							<th>Tanggal Berakhir</th>
							<th>Action</th>
						</tr>
						<?php
							foreach ($accreditations as $key => $accreditation) {
						?>
							<tr>
								<td><?php echo $key+1; ?></td>
								<td><?php echo $accreditation->department; ?></td>
								<td><?php echo $accreditation->sk_number; ?></td>
								<td><?php echo $accreditation->accreditation; ?></td>
								<td><?php echo $accreditation->date_end; ?></td>
								<td><a href="<?php echo base_url('admin/accreditation/'.$accreditation->id); ?>" class="btn btn-primary btn-xs">Detail</a></td>
							</tr>
						<?php		
							}
						?>
					</table>
				</div>
			</div>
		</div>
	</div>
