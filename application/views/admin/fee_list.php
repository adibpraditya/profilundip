<div class="content-wrapper">
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<?php
				
				if($this->session->flashdata('msg')){
					echo $this->session->flashdata('msg');
				}
				?>
				<h3>Daftar Biaya Kuliah</h3>
				<table class="table">
					<tr>
						<th>#</th>
						<th>Program Studi</th>
						<th>Action</th>
					</tr>
					<?php
						foreach ($fees as $key => $fee) {
					?>
						<tr>
							<td><?php echo $key+1; ?></td>
							<td><?php echo $fee->department; ?></td>
							<td><a href="<?php echo base_url('admin/fee/'.$fee->id_prodi); ?>" class="btn btn-primary btn-xs">Detail</a></td>
						</tr>
					<?php		
						}
					?>
				</table>
			</div>
		</div>
	</div>
</div>