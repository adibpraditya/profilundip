<div class="content-wrapper">
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="content-header">
					<h2>
						Edit Daftar Peminat
						<?php echo form_open_multipart(base_url().'admin/animo/'.$peminat->id.'/delete'); ?>

						<button type="submit" class="btn btn-danger"
							onclick="return confirm('Anda yakin akan menghapus data peminat ini?');"
						>
							Hapus
						</button>
						<?php echo form_close(); ?>
					</h2>
				</div>
			</div>
			<div class="row">
				<?php
					if($this->session->flashdata('msg')){
						echo $this->session->flashdata('msg');
					}
				?>
				
				<?php echo form_open_multipart(base_url().'admin/animo/'.$peminat->id.'/update', array('id'=>'create')); ?>
				
				<div class="form-group">
					<label for="customer" class="control-label">
						Program Studi
					</label>
					<div class="control-input ">
						<select name="prodi" class="form-control" id="prodi">
							<option value="">-Pilih Prodi-</option>
							<?php
								foreach($departments as $department){
							?>
								<option class="<?php echo $department->id_fakultas;?>" value="<?php echo $department->id_jurusan;?>" <?php echo $department->id_jurusan == $peminat->id_prodi ? "selected" : ""; ?>>
									<?php echo $department->nama_jurusan;?>
								</option>
							<?php	
									
								}
							?>
						</select>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Tahun', 'tahun');
						echo form_input(['name'=>'tahun', 'value'=>set_value('tahun', $peminat->tahun), 'class'=>'form-control']);
						echo form_error('tahun'); 
					?>
					</div>
				</div>

				<div class="row form-group col-md-4">
					<div class="control-input">
					<?php
						echo form_label('Daya Tampung SNMPTN', 'dt_snmptn');
						echo form_input(['name'=>'dt_snmptn', 'value'=>set_value('dt_snmptn', $peminat->dt_snmptn), 'class'=>'form-control']);
						echo form_error('dt_snmptn'); 
					?>
					</div>
				</div>

				<div class="form-group col-md-4">
					<div class="control-input">
					<?php
						echo form_label('Daya Tampung SBMPTN', 'dt_snmptn');
						echo form_input(['name'=>'dt_sbmptn', 'value'=>set_value('dt_sbmptn', $peminat->dt_sbmptn), 'class'=>'form-control']);
						echo form_error('dt_sbmptn'); 
					?>
					</div>
				</div>

				<div class="form-group col-md-4">
					<div class="control-input">
					<?php
						echo form_label('Daya Tampung UM', 'dt_um');
						echo form_input(['name'=>'dt_um', 'value'=>set_value('dt_um', $peminat->dt_um), 'class'=>'form-control']);
						echo form_error('dt_um'); 
					?>
					</div>
				</div>

				<div class="row form-group col-md-4">
					<div class="control-input">
					<?php
						echo form_label('Animo Pendaftar SNMPTN', 'an_snmptn');
						echo form_input(['name'=>'an_snmptn', 'value'=>set_value('an_snmptn', $peminat->an_snmptn), 'class'=>'form-control']);
						echo form_error('an_snmptn'); 
					?>
					</div>
				</div>

				<div class="form-group col-md-4">
					<div class="control-input">
					<?php
						echo form_label('Animo Pendaftar SBMPTN', 'an_snmptn');
						echo form_input(['name'=>'an_sbmptn', 'value'=>set_value('an_sbmptn', $peminat->an_sbmptn), 'class'=>'form-control']);
						echo form_error('an_sbmptn'); 
					?>
					</div>
				</div>

				<div class="form-group col-md-4">
					<div class="control-input">
					<?php
						echo form_label('Animo Pendaftar UM', 'an_um');
						echo form_input(['name'=>'an_um', 'value'=>set_value('an_um', $peminat->an_um), 'class'=>'form-control']);
						echo form_error('an_um'); 
					?>
					</div>
				</div>

				<div class="row form-group col-md-4">
					<div class="control-input">
					<?php
						echo form_label('Mahasiswa Diterima SNMPTN', 'te_snmptn');
						echo form_input(['name'=>'te_snmptn', 'value'=>set_value('te_snmptn', $peminat->te_snmptn), 'class'=>'form-control']);
						echo form_error('te_snmptn'); 
					?>
					</div>
				</div>

				<div class="form-group col-md-4">
					<div class="control-input">
					<?php
						echo form_label('Mahasiswa Diterima SBMPTN', 'te_sbmptn');
						echo form_input(['name'=>'te_sbmptn', 'value'=>set_value('te_sbmptn', $peminat->te_sbmptn), 'class'=>'form-control']);
						echo form_error('te_sbmptn'); 
					?>
					</div>
				</div>

				<div class="form-group col-md-4">
					<div class="control-input">
					<?php
						echo form_label('Mahasiswa Diterima UM', 'te_um');
						echo form_input(['name'=>'te_um', 'value'=>set_value('te_um', $peminat->te_um), 'class'=>'form-control']);
						echo form_error('te_um'); 
					?>
					</div>
				</div>
				
				<div class="form-group">
					<?php echo form_submit(['name'=>'kirim', 'value'=>'Simpan', 'class'=>'btn btn-primary']); ?>
				</div>
				
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>