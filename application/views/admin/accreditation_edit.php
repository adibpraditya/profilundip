<div class="content-wrapper">
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="content-header">
					<h2>
						Edit accreditation
						<?php echo form_open_multipart(base_url().'admin/accreditation/'.$accreditation->id.'/delete'); ?>

						<button type="submit" class="btn btn-danger"
							onclick="return confirm('Anda yakin akan menghapus data kuliner ini?');"
						>
							Hapus
						</button>
						<?php echo form_close(); ?>
					</h2>
				</div>
			</div>
			<div class="row">
				<?php
					if($this->session->flashdata('msg')){
						echo $this->session->flashdata('msg');
					}
				?>
				
				<?php echo form_open_multipart(base_url().'admin/accreditation/'.$accreditation->id.'/update', array('id'=>'create')); ?>
				
				<div class="form-group">
					<label for="customer" class="control-label">
						Program Studi
					</label>
					<div class="control-input ">
						<select name="prodi" class="form-control" id="prodi">
							<option value="">-Pilih Prodi-</option>
							<?php
								foreach($departments as $department){
							?>
								<option class="<?php echo $department->id_fakultas;?>" value="<?php echo $department->id_jurusan;?>" <?php echo $department->id_jurusan == $accreditation->id_prodi ? "selected" : ""; ?>>
									<?php echo $department->nama_jurusan;?>
								</option>
							<?php	
									
								}
							?>
						</select>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Nomor SK Akreditasi', 'sk');
						echo form_input(['name'=>'sk', 'value'=>set_value('sk', $accreditation->sk_number), 'class'=>'form-control']);
						echo form_error('sk'); 
					?>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Akreditasi', 'accreditation');
						echo form_input(['name'=>'accreditation', 'value'=>set_value('accreditation', $accreditation->accreditation), 'class'=>'form-control']);
						echo form_error('accreditation'); 
					?>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Tanggal Berakhir', 'date_end');
						echo form_input(['name'=>'date_end', 'value'=>set_value('date_end', $accreditation->date_end), 'class'=>'form-control', 'type'=>'date']);
						echo form_error('date_end'); 
					?>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
						<img src="<?php echo base_url().'/upload/accreditation/'.$accreditation->image; ?>" width="200px">
					</div>
					<div class="control-input">
					<?php
						echo form_label('Image', 'image');
						echo form_upload(['name'=>'image', 'value'=>set_value('image')]);
						echo form_error('image'); 
					?>
					</div>
				</div>
				
				<div class="form-group">
					<?php echo form_submit(['name'=>'kirim', 'value'=>'Simpan', 'class'=>'btn btn-primary']); ?>
				</div>
				
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>