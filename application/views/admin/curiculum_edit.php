<style>
	textarea {
    	height: 150px !important;
	}
</style>
<div class="content-wrapper">
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="content-header">
					<h2>
						Edit curiculum
						<?php echo form_open_multipart(base_url().'admin/curiculum/'.$curiculum->id.'/delete'); ?>

						<button type="submit" class="btn btn-danger"
							onclick="return confirm('Anda yakin akan menghapus data kuliner ini?');"
						>
							Hapus
						</button>
						<?php echo form_close(); ?>
					</h2>
				</div>
			</div>
			<div class="row">
				<?php
					if($this->session->flashdata('msg')){
						echo $this->session->flashdata('msg');
					}
				?>
				
				<h4>Add curiculum</h4>

				<?php echo form_open_multipart(base_url().'admin/curiculum/'.$curiculum->id.'/update', array('id'=>'create')); ?>
				
				<div class="form-group">
					<label for="customer" class="control-label">
						Program Studi
					</label>
					<div class="control-input ">
						<select name="prodi" class="form-control" id="prodi">
							<option value="">-Pilih Prodi-</option>
							<?php
								foreach($departments as $department){
							?>
								<option class="<?php echo $department->id_fakultas;?>" value="<?php echo $department->id_jurusan;?>" <?php echo $department->id_jurusan == $curiculum->id_prodi ? "selected" : ""; ?>>
									<?php echo $department->nama_jurusan;?>
								</option>
							<?php	
									
								}
							?>
						</select>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Izin Penyelenggaraan', 'izin');
						echo form_input(['name'=>'izin', 'value'=>set_value('izin', $curiculum->izin), 'class'=>'form-control']);
						echo form_error('izin'); 
					?>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Gelar', 'gelar');
						echo form_input(['name'=>'gelar', 'value'=>set_value('gelar', $curiculum->gelar), 'class'=>'form-control']);
						echo form_error('gelar'); 
					?>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('KKNI', 'kkni');
						echo form_input(['name'=>'kkni', 'value'=>set_value('kkni', $curiculum->kkni), 'class'=>'form-control']);
						echo form_error('kkni'); 
					?>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Tujuan', 'tujuan');
						echo $this->ckeditor->editor("tujuan",$curiculum->tujuan);
						echo form_error('tujuan'); 
					?>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Materi', 'materi');
						echo $this->ckeditor->editor("materi",$curiculum->materi);
						echo form_error('materi'); 
					?>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Kompetensi Lulusan', 'kompetensi');
						echo $this->ckeditor->editor("kompetensi",$curiculum->kompetensi);
						echo form_error('kompetensi'); 
					?>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Metode Pembelajaran', 'pembelajaran');
						echo $this->ckeditor->editor("pembelajaran",$curiculum->pembelajaran);
						echo form_error('pembelajaran'); 
					?>
					</div>
				</div>
				
				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Struktur dan Persyaratan', 'persyaratan');
						echo $this->ckeditor->editor("persyaratan",$curiculum->persyaratan);
						echo form_error('persyaratan'); 
					?>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Dukungan', 'dukungan');
						echo $this->ckeditor->editor("dukungan",$curiculum->dukungan);
						echo form_error('dukungan'); 
					?>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Syarat Admisi', 'syarat_admisi');
						echo $this->ckeditor->editor("syarat_admisi",$curiculum->syarat_admisi);
						echo form_error('syarat_admisi'); 
					?>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Lain-lain', 'lain_lain');
						echo $this->ckeditor->editor("lain_lain",$curiculum->lain_lain);
						echo form_error('lain_lain'); 
					?>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Video', 'video');
						echo form_input(['name'=>'video', 'value'=>set_value('video', $curiculum->video), 'class'=>'form-control']);
						echo form_error('video'); 
					?>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Profil Lulusan', 'profil');
					?>
					
					<table class="table" style="width:50%">
						<thead>
							<tr>
								<th>#</th>
								<th>Profesi</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody id="tbody_profesi">
							
						</tbody>
					</table>
					</div>
				</div>
				
				<input type="hidden" value="[]" id="profesi" name="profesi">
				
				<div class="form-group">
					<?php echo form_submit(['name'=>'kirim', 'value'=>'Simpan', 'class'=>'btn btn-primary']); ?>
				</div>
				
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>

<script type="x-tmpl-mustache" id="profesi-table">
	<tr>
		<td>
		</td>
		<td>
			<select name="profiles" class="form-control" id="profiles">
				<option value="">-Pilih Profesi-</option>
				<?php
					foreach($profiles as $profile){
				?>
					<option class="" value="<?php echo $profile->id;?>" ><?php echo $profile->profesi;?></option>
				<?php	
						
					}
				?>
			</select>
		</td>
		<td>
			<button type="button" class="btn btn-info" id="tambah">Tambah</button>
		</td>
	</tr>
	{% #item %}
		<tr>
			<td>
				{% no %}
			</td>
			<td>
				{% profesi %}
			</td>
			<td>
				<button type="button" id="delete_{% _id %}" class="btn btn-danger btn-xs btn-delete-item" data-id="{% _id %}">hapus</button>
			</td>
		</tr>
	{% /item %}
</script>

<script>
	Mustache.tags = ['{%', '%}'];
	var profesi = JSON.parse($('#profesi').val());
	console.log(profesi);

	function render(){
		getIndex();
		$('#profesi').val(JSON.stringify(profesi));
		//console.log(profesi);
		var tmpl = $('#profesi-table').html();
		//console.log(tmpl);
		Mustache.parse(tmpl);
		var html = Mustache.render(tmpl, { item : profesi });
		$('#tbody_profesi').html(html);

		bind();
	}

	function bind(){
		$('#tambah').on('click', tambah);
		$('.btn-delete-item').on('click', del);
	}

	function getIndex() 
	{
		for (idx in profesi) {
			profesi[idx]['_id'] = idx;
			profesi[idx]['no'] = parseInt(idx) + 1;
		}
	}

	function tambah(){
		var id = $('#profiles').val();
		var prof_text = $('#profiles option:selected').text();

		if (!(id)){
			alert("Profesi belum dipilih");
			return;
		}

		for (idx in profesi) {
			if(profesi[idx]['id_prof'] == id){
				alert("Profesi sudah ditambahkan");
				return;
			}
		}

		var input = { 
			'profesi' : prof_text,
			'id_prof' : id
		}
		profesi.push(input);
		render();
	}

	function del() 
	{
		var i = parseInt($(this).data('id'), 10);
		
		profesi.splice(i, 1);
		render();
	}
	
	function profesi_validasi(){
		var profesi = JSON.parse($('#kompetensi_dasar').val());
		console.log(profesi.length);
		if(profesi.length < 1){
			$('#profesi').addClass('alert alert-danger');
			alert('Minimal Ada 1 Kompetensi Dasar');

    		return false;
		}
	}

	render();
</script>