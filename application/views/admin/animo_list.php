<div class="content-wrapper">
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<?php
				
				if($this->session->flashdata('msg')){
					echo $this->session->flashdata('msg');
				}
				?>
				<h3>Daftar Peminat</h3>
				<table class="table">
					<tr>
						<th>#</th>
						<th>Program Studi</th>
						<th>Action</th>
					</tr>
					<?php
						foreach ($peminat as $key => $pem) {
					?>
						<tr>
							<td><?php echo $key+1; ?></td>
							<td><?php echo $pem->department; ?></td>
							<td><a href="<?php echo base_url('admin/animo/'.$pem->id); ?>" class="btn btn-primary btn-xs">Detail</a></td>
						</tr>
					<?php		
						}
					?>
				</table>
			</div>
		</div>
	</div>
</div>