<div class="content-wrapper">
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<?php
					
					if($this->session->flashdata('msg')){
						echo $this->session->flashdata('msg');
					}
				?>
				
				<h4>Add Culinary</h4>

				<?php echo form_open_multipart(base_url().'admin/culinary/store', array('id'=>'create')); ?>
				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Title', 'title');
						echo form_input(['name'=>'title', 'value'=>set_value('title'), 'class'=>'form-control']);
						echo form_error('title'); 
					?>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Location', 'location');
						echo form_input(['name'=>'location', 'value'=>set_value('location'), 'class'=>'form-control']);
						echo form_error('location'); 
					?>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Position', 'position');
						echo form_input(['name'=>'position', 'value'=>set_value('position'), 'class'=>'form-control']);
						echo form_error('position'); 
					?>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Description', 'description');
						echo form_textarea(['name'=>'description', 'value'=>set_value('description'), 'class'=>'form-control']);
						echo form_error('description'); 
					?>
					</div>
				</div>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Image', 'image');
						echo form_upload(['name'=>'image', 'value'=>set_value('image')]);
						echo form_error('image'); 
					?>
					</div>
				</div>
				
				<div class="form-group">
					<?php echo form_submit(['name'=>'kirim', 'value'=>'Simpan', 'class'=>'btn btn-primary']); ?>
				</div>
				
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>