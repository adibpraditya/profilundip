	<div class="content-wrapper">
		<div class="content">
			<div class="container-fluid">
				<div class="row">
				<?php
					
					if($this->session->flashdata('msg')){
						echo $this->session->flashdata('msg');
					}
				?>
				
				<h4>Tambah Profil Lulusan</h4>

				<?php echo form_open_multipart($form, array('id'=>'create')); ?>

				<div class="form-group">
					<div class="control-input">
					<?php
						echo form_label('Profesi', 'profesi');
						echo form_input(['name'=>'profesi', 'value'=>set_value('profesi', (isset($profile)) ? $profile->profesi : ""), 'class'=>'form-control']);
						echo form_error('profesi'); 
					?>
					</div>
				</div>
				
				<div class="form-group">
					<?php echo form_submit(['name'=>'kirim', 'value'=>'Simpan', 'class'=>'btn btn-primary']); ?>
				</div>
				
				<?php echo form_close(); ?>
			</div>
				<div class="row">
					<h4>Daftar Profil Lulusan</h4>
					<table class="table">
						<tr>
							<th>#</th>
							<th>Profil Alumni</th>
							<th>Aksi</th>
						</tr>
						<?php
							foreach ($profiles as $key => $profile) {
						?>
							<tr>
								<td><?php echo $key+1; ?></td>
								<td><?php echo $profile->profesi; ?></td>
								<td>
									<a href="<?php echo base_url('admin/alumni/'.$profile->id); ?>" class="btn btn-primary btn-xs">Edit</a>
									<a href="<?php echo base_url('admin/alumni/'.$profile->id.'/delete'); ?>" class="btn btn-danger btn-xs" onclick="return confirm('Anda yakin akan menghapus data profil alumni ini?');">Delete</a>
								</td>
							</tr>
						<?php		
							}
						?>
					</table>
				</div>
			</div>
		</div>
	</div>
