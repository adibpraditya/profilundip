<div class="clear"></div>
<!-- Slider -->
<style>
  .flex-control-nav{
    display:none;
  }
</style>
<div class="bannerbg">
  <div class="container clearfix">
    <div class="flexslider" >
      <ul class="slides">
        <li> <img src="<?php echo base_url()?>/assets/images/fslide01.jpg" alt="">
          <p class="flex-caption">I am Caption!</p>
        </li>
        <li> <a href="#"><img src="<?php echo base_url()?>/assets/images/fslide02.jpg" alt=""></a> </li>
        <li> <img src="<?php echo base_url()?>/assets/images/fslide03.jpg" alt="">
          <p class="flex-caption">I am Caption!</p>
        </li>
        <li> <img src="<?php echo base_url()?>/assets/images/fslide04.jpg" alt=""> </li>
        <li> <img src="<?php echo base_url()?>/assets/images/fslide05.jpg" alt=""> </li>
        <li> <img src="<?php echo base_url()?>/assets/images/fslide06.jpg" alt=""> </li>
      </ul>
    </div>
  </div>
</div>
<!-- /Slider -->
<div class="clear padding40"></div>
<!-- Content -->
<section class="container clearfix">
  <!-- START FEATURED COLUMNS -->
  <?php
	$i=0;
	foreach($fakultas as $fakul){
		$i++;
  ?>
  <div class="col_1_3 <?php echo ($i % 3 == 0) ? 'last' : ''?>">
    <div class="features">
      <div class="title clearfix" style="height:50px" align="center"> 
        <h3><?php echo $fakul->nama_fakultas?></h3>
      </div>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id ligula in ipsum vulputate volutpat at ut nisi. Aliquam erat volutpat. Maecenas pretium dolor vitae lectus fermentum convallis bibendum in erat. Donec vitae risus non lorem volutpat fringilla in in metus. Aenean quis eros diam. Proin porta quam at neque congue iaculis. <br />
        <a href="#">more info</a></p>
    </div>
  </div>
  <?php
	}
  ?>
  
  <!-- END FEATURED COLUMNS -->
  <div class="clear padding10"></div>
  <div class="recent_works_left">
    <h2 class="red">Featured Work</h2>
  </div>
  <div class="recent_works_arrows"> <a href="#" class="prev_item"></a><a href="#" class="next_item"></a> </div>
  <div class="clear"></div>
  <div class="line"></div>
  <div class="clear padding30"></div>
  <div class="recent_works">
    <ul id="work">
      <li id="id-1" class="app"> <span class="recent_image"> <a href="<?php echo base_url()?>/assets/images/portfolio_large.jpg" data-rel="prettyPhoto" class="img-thumb"><img class="portfolio_image" src="<?php echo base_url()?>/assets/images/featured_work_1.jpg" alt=""></a> </span> <span class="title"><a href="portfolio-details.html">Open Source Icons</a></span> <span class="clear padding15"></span> </li>
      <li id="id-2" class="util"> <span class="recent_image"> <a href="<?php echo base_url()?>/assets/images/portfolio_large.jpg" data-rel="prettyPhoto"  class="img-thumb"><img class="portfolio_image" src="<?php echo base_url()?>/assets/images/featured_work_2.jpg" alt=""></a> </span> <span class="title"><a href="portfolio-details.html">Free Admin Template</a></span> <span class="clear padding15"></span> </li>
      <li id="id-3" class="app"> <span class="recent_image"> <a href="<?php echo base_url()?>/assets/images/portfolio_large.jpg" data-rel="prettyPhoto" class="img-thumb"><img class="portfolio_image" src="<?php echo base_url()?>/assets/images/featured_work_3.jpg" alt=""></a> </span> <span class="title"><a href="portfolio-details.html">Free Social Plugin</a></span> <span class="clear padding15"></span> </li>
      <li id="id-4" class="app"> <span class="recent_image"> <a href="<?php echo base_url()?>/assets/images/portfolio_large.jpg" data-rel="prettyPhoto" class="img-thumb"><img class="portfolio_image" src="<?php echo base_url()?>/assets/images/portfolio_image_4.jpg" alt=""></a> </span> <span class="title"><a href="portfolio-details.html">Desing Freebies</a></span> <span class="clear padding15"></span> </li>
      <li id="id-5" class="app"> <span class="recent_image"> <a href="<?php echo base_url()?>/assets/images/portfolio_large.jpg" data-rel="prettyPhoto" class="img-thumb"><img class="portfolio_image" src="<?php echo base_url()?>/assets/images/portfolio_image_5.jpg" alt=""></a> </span> <span class="title"><a href="portfolio-details.html">Free WordPress Resume Theme</a></span> <span class="clear padding15"></span> </li>
      <li id="id-6" class="app"> <span class="recent_image"> <a href="<?php echo base_url()?>/assets/images/portfolio_large.jpg" data-rel="prettyPhoto" class="img-thumb"><img class="portfolio_image" src="<?php echo base_url()?>/assets/images/portfolio_image_6.jpg" alt=""></a> </span> <span class="title"><a href="portfolio-details.html">jQuery Pagination Plugin</a></span> <span class="clear padding15"></span> </li>
      <li id="id-7" class="app"> <span class="recent_image"> <a href="<?php echo base_url()?>/assets/images/portfolio_large.jpg" data-rel="prettyPhoto" class="img-thumb"><img class="portfolio_image" src="<?php echo base_url()?>/assets/images/portfolio_image_7.jpg" alt=""></a> </span> <span class="title"><a href="portfolio-details.html">Clean Business HTML Template</a></span> <span class="clear padding15"></span> </li>
    </ul>
  </div>
</section>
<!-- /Content -->
<section class="homepage_widgets_bg clearfix">
  <div class="container clearfix">
    <div class="padding20"></div>
    <!-- START COL 1/2 -->
    <div class="col_1_2 ">
      <h1 class="regular white bottom_line">About Us</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus convallis urna a enim convallis a bibendum lectus scelerisque. Aenean at mauris augue, sed fringilla justo. Proin et porta ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus convallis urna a enim convallis a bibendum lectus scelerisque. Aenean at mauris augue, sed fringilla justo. Proin et porta ligula.</p>
      <p>Quisque malesuada lorem at leo volutpat tristique. Nunc sit amet felis imperdiet ante tincidunt viverra sit amet quis ipsum. Aenean metus urna, placerat ut pharetra id, pharetra eget turpis. Sed blandit nunc eget lorem vestibulum malesuada.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus convallis urna a enim convallis a bibendum lectus scelerisque. Aenean at mauris augue, sed fringilla justo. Proin et porta ligula. Quisque malesuada lorem at leo volutpat tristique. Nunc sit amet felis imperdiet ante tincidunt viverra sit amet quis ipsum. </p>
    </div>
    <!-- END COL 1/2 -->
    <!-- START COL 1/2 -->
    <div class="col_1_2 last" id="why-section">
      <h1 class="regular white bottom_line">Why Us</h1>
      <div><a href="#"><img class="alignleft MT0" id="why1" src="<?php echo base_url()?>/assets/images/content-img1.png" alt=""></a></div>
      <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus convallis urna a enim convallis a bibendum lectus scelerisque. Aenean at mauris augue, sed fringilla justo. </p>
      <div class="clear padding10"></div>
      <div><a href="#"><img class="alignleft MT0" id="why2" src="<?php echo base_url()?>/assets/images/content-img2.png" alt=""></a></div>
      <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus convallis urna a enim convallis a bibendum lectus scelerisque. Aenean at mauris augue, sed fringilla justo. </p>
      <div class="clear padding10"></div>
      <div><a href="#"><img class="alignleft MT0" id="why3" src="<?php echo base_url()?>/assets/images/content-img3.png" alt=""></a></div>
      <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus convallis urna a enim convallis a bibendum lectus scelerisque. Aenean at mauris augue, sed fringilla justo. </p>
    </div>
    <div class="clear padding10"></div>
    <!-- end col 1/2 -->
    <!-- start col -->
    <div>
      <h2 class="regular white bottom_line">All-in-one Template</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus convallis urna a enim convallis a bibendum lectus scelerisque. Aenean at mauris augue, sed fringilla justo. Proin et porta ligula. </p>
      <div class="clearfix"> <a href="#" class="green-button">Buy This Template</a> </div>
    </div>
    <!-- emd col -->
    <div class="clear padding80"></div>
  </div>
</section>