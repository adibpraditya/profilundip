<!DOCTYPE html>
<html lang="en">
<head>
<title>Brownie</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
<link rel="stylesheet" href="<?php echo base_url().'assets/css/prettyPhoto.css'?>" type="text/css">
<link rel="stylesheet" href="<?php echo base_url().'assets/css/flexslider.css'?>" type="text/css">
<link rel="stylesheet" href="<?php echo base_url().'assets/css/style2.css'?>" type="text/css">
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/selectivizr-min.js"></script>
<link rel="stylesheet" href="css/ie.css" type="text/css">
<![endif]-->
<script src="<?php echo base_url().'assets/js/jquery.min.js'?>"></script>
<script src="<?php echo base_url().'assets/js/jquery.easing.1.3.js'?>"></script>
<script src="<?php echo base_url().'assets/js/jquery-ui-1.8.16.custom.min.js'?>"></script>
<script src="<?php echo base_url().'assets/js/all-in-one-min.js'?>"></script>
<script src="<?php echo base_url().'assets/js/setup.js'?>"></script>
<script>
$(window).load(function () {
    $('.flexslider').flexslider();
});

$(function () {
    $('#work').carouFredSel({
        width: '100%',
        scroll: 1,
        auto: false,
        pagination: false,
        prev: '.prev_item',
        next: '.next_item'
    });

    $("#work").touchwipe({
        wipeLeft: function () { $('.next_item').trigger('click'); },
        wipeRight: function () { $('.prev_item').trigger('click'); }
    });
});

$(window).load(function(){
	$('#demo-side-bar').removeAttr('style');
});
</script>
<style type="text/css">
.demobar {
	display:none;
}
#demo-side-bar {
	top:53px!important;
	left:90%!important;
	display:block!important;
}
</style>
</head>
<body>
<header class="header_bg clearfix">
  <div class="container clearfix">
    <!-- Social -->
    <ul class="social-links">
      <li><a href="#"><img src="<?php echo base_url()?>/assets/images/facebook.png" alt=""></a></li>
      <li><a href="#"><img src="<?php echo base_url()?>/assets/images/twitter.png" alt=""></a></li>
    </ul>
    <!-- /Social -->
    <!-- Logo -->
    <div class="logo"> <a href="index.html"><img src="<?php echo base_url()?>/assets/images/logo.png" alt=""></a> </div>
    <!-- /Logo -->
    <!-- Master Nav -->
    <nav class="main-menu">
      <ul>
        <li><a href="index.html">Home</a></li>
        <li> <a>Pages</a>
          <ul>
            <li><a href="elements.html">Elements</a></li>
            <li><a href="typography.html">Typography</a></li>
            <li><a href="blog-single.html">Blog Single Post</a></li>
            <li><a href="#">Pricing</a>
              <ul>
                <li><a href="pricing-2-cols.html">Pricing 2 Cols</a></li>
                <li><a href="pricing-3-cols.html">Pricing 3 Cols</a></li>
                <li><a href="pricing-4-cols.html">Pricing 4 Cols</a></li>
                <li><a href="pricing-5-cols.html">Pricing 5 Cols</a></li>
              </ul>
            </li>
            <li><a href="full-width.html">Full Width</a></li>
            <li><a href="404.html">404 Page</a></li>
          </ul>
        </li>
        <li><a>Portfolio</a>
          <ul>
            <li><a href="portfolio-2-cols.html">Portfolio 2 Cols</a></li>
            <li><a href="portfolio-3-cols.html">Portfolio 3 Cols</a></li>
            <li><a href="portfolio-4-cols.html">Portfolio 4 Cols</a></li>
            <li><a href="portfolio-details.html">Portfolio Details</a></li>
          </ul>
        </li>
        <li><a href="blog.html">Blog</a></li>
        <li><a href="contact.html">Contact</a></li>
      </ul>
    </nav>
    <!-- /Master Nav -->
  </div>
</header>