<footer class="footer_bg_bottom clearfix">
  <div class="footer_bottom container">
    <div class="col_2_3">
      <div class="menu">
        <ul>
          <li><a href="index.html">Home</a></li>
          <li><a href="elements.html">Elements</a></li>
          <li><a href="portfolio-4-cols.html">Portfolio</a></li>
          <li><a href="pricing-5-cols.html">Pricing</a></li>
          <li><a href="blog.html">Blog</a></li>
          <li><a href="contact.html">Contact</a></li>
        </ul>
      </div>
      <div class="clear padding20"></div>
      <p>&copy; Copyright 2012 <a href="#">Company Name</a> - All Rights Reserved. | Website Template By <a target="_blank" href="http://www.egrappler.com/">EGrappler</a></p>
    </div>
    <div class="clear padding20"></div>
  </div>
</footer>
<!-- /footer -->
<div id="demo-side-bar"> </div>
<!--wrapper end-->
</body>
</html>