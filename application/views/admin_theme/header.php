
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Profil Program Studi Admin</title>

	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<script src="<?php echo base_url();?>assets/js/scriptku.js"></script>
	
</head>
<body>
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header logo-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">
					Profil Program Studi Admin
				</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li>
						<a href="<?php echo base_url();?>admin/curiculum">Kurikulum</a>
					</li>
					<li>
						<a href="<?php echo base_url();?>admin/alumni">Profil Lulusan</a>
					</li>
					<li>
						<a href="<?php echo base_url();?>admin/accreditation">Akreditasi</a>
					</li>
					<li>
						<a href="<?php echo base_url();?>admin/fee">Biaya Kuliah</a>
					</li>
					<li>
						<a href="<?php echo base_url();?>admin/animo">Peminat</a>
					</li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Akun <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li class="dropdown-header"><?php echo $this->session->userdata('name'); ?></li>
							<li><a href="<?php echo base_url('admin/auth/logout') ?>">Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="page-wrapper">

	
