<?php 
	class MainTheme{
		public $_ci;
		function __construct()
		{
			$this->_ci =&get_instance();
		}
		
		function display($content=null, $data=null)
		{
			$data['_header']=$this->_ci->load->view('main_theme/header',$data, true);
			$data['_footer']=$this->_ci->load->view('main_theme/footer',$data, true);
			$data['_content']=$this->_ci->load->view($content,$data, true);
			$this->_ci->load->view('main_theme/page.php',$data);
		}
	}
	
// end of file template.php
